<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<link rel="stylesheet" type="text/css" href="webStyle.css">
	</head>
	<body>
		<div>
			<header class="headerWeb">
				<div><h1>VOLLITIC</h1></div>
				<div><h3><u>PAID TRANSACTION</u></h3></div>
				<div><script type="text/javascript">document.write(Date())</script></div>
			</header>
			<nav class="menuWeb">
				<button><a href="orderPortal.php">ORDER</a></button>
				<button><a href="shoppingCartPortal.php">SHOPPING CART</a></button>				
				<button><a href="paymentPortal.php">PAYMENT</a></button>
				<button><a href="paidTransactionPortal.php">PAID TRANSACTION</a></button>
			</nav>
			<div class="containerWeb">
				<?php 
					include 'connection.php';
					$sqlSelectPerItem = "SELECT pt.payment_date, pt.code, i.image, i.name, pt.price_per_item, pt.order_quantity
					FROM paid_transaction pt join items i
					on pt.code = i.code";
					$result=mysqli_query($conn,$sqlSelectPerItem);
				?>
				<table>
					<?php
					if ($result->num_rows > 0) {
					    // output data of each row
					    while($row=mysqli_fetch_assoc($result)) {
					    	echo "
					    	<tr>
					    		<td><img src=\"".$row["image"]."\"></td>
				        	</tr>
				        	<tr>
				        		<td><b>ID</b></td>
				        		<td>".$row["code"]."</td>
				        		<td><b>Payment Date</b></td>
				        		<td>".$row["payment_date"]."</td>
				        		<td><b>Name</b></td>
				        		<td>".$row["name"]."</td>
				        	</tr>
				        	<tr>
				        		<td><b>Price per Item</b></td>
				        		<td>Rp.".$row["price_per_item"].",00</td>
				        		<td><b>Order Quantity</b></td>
				        		<td>".$row["order_quantity"]."</td>
				        		<td><b>Sub Total</b></td>
				        		<td>Rp.".$row["price_per_item"]*$row["order_quantity"].",00</td>
							</tr>
				        	";
					    }
					    $sqlSelectInfo = "SELECT DISTINCT email, phone, address, total_item, total_price, account, bank_name
						FROM paid_transaction";
						$result=mysqli_query($conn,$sqlSelectInfo);
						while($row=mysqli_fetch_assoc($result)) {
					?>
							<tr>
				        		<td colspan="2"></td>
				        		<td><b>Total Item</b></td>
				        		<td><?php echo $row["total_item"]; ?></td>
				        		<td><b>Total Price</b></td>
				        		<td><?php echo "Rp.".$row["total_price"].",00"; ?></td>
				        	</tr>
							<tr>
								<td><b>Email</b></td>
								<td><?php echo $row["email"]; ?></td>
								<td><b>Phone</b></td>
								<td><?php echo $row["phone"]; ?></td>
								<td><b>Address</b></td>
								<td><?php echo $row["address"]; ?></td>
							</tr>
							<tr>
								<td><b>Account Number</b></td>
								<td><?php echo $row["account"]; ?></td>
								<td><b>Bank Name</b></td>
								<td><?php echo $row["bank_name"]; ?></td>
							</tr>
					<?php
						}
					} else {
					    echo "<font color=\"red\">There is no any paid transaction.</font>";
					}
					?>
				</table>
				<?php $conn->close(); ?>
				<script type="text/javascript">
					function clickPayNow(){
						document.getElementById("formPayment").submit();
					}
				</script>
			</div>
		</div>
	</body>
</html>