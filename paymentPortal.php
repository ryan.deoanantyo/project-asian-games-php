<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<link rel="stylesheet" type="text/css" href="webStyle.css">
	</head>
	<body>
		<div>
			<header class="headerWeb">
				<div><h1>VOLLITIC</h1></div>
				<div><h3><u>PAYMENT</u></h3></div>
				<div><script type="text/javascript">document.write(Date())</script></div>
			</header>
			<nav class="menuWeb">
				<button><a href="orderPortal.php">ORDER</a></button>
				<button><a href="shoppingCartPortal.php">SHOPPING CART</a></button>				
				<button><a href="paymentPortal.php">PAYMENT</a></button>
				<button><a href="paidTransactionPortal.php">PAID TRANSACTION</a></button>
			</nav>
			<div class="containerWeb">
				<?php 
					include 'connection.php';
					$sql = "SELECT o.code, o.order_date, i.name, i.image, i.price, i.quantity, o.order_quantity 
					FROM `order` o JOIN items i 
					WHERE o.code = i.code";
					$result=mysqli_query($conn,$sql);
				?>
				<table>
					<?php
					$totalItems = 0;
					$totalPrice = 0;
					if ($result->num_rows > 0) {
					    // output data of each row
					    while($row=mysqli_fetch_assoc($result)) {
					    	$totalItems = $totalItems + $row["order_quantity"];
					    	$totalPrice = $totalPrice + $row["price"] * $row["order_quantity"];
							echo "
					    	<tr>
					    		<td><img src=\"".$row["image"]."\"></td>
				        	</tr>
				        	<tr>
				        		<td><b>ID</b></td>
				        		<td>".$row["code"]."</td>
				        		<td><b>Order Date</b></td>
				        		<td>".$row["order_date"]."</td>
				        		<td><b>Name</b></td>
				        		<td>".$row["name"]."</td>
				        	</tr>
				        	<tr>
				        		<td><b>Price per Item</b></td>
				        		<td>Rp.".$row["price"].",00</td>
				        		<td><b>Order Quantity</b></td>
				        		<td>".$row["order_quantity"]."</td>
				        		<td><b>Sub Total</b></td>
				        		<td>Rp.".$row["price"]*$row["order_quantity"].",00</td>
							</tr>
				        	";
					    }
					?>
						<form action="handlePayment.php" method="post" id="formPayment">
							<tr>
				        		<td colspan="2"></td>
				        		<td><b>Total Item</b></td>
				        		<td><?php echo $totalItems; ?></td>
				        		<input type="hidden" name="totalItems" value="<?php echo $totalItems; ?>">
				        		<td><b>Total Price</b></td>
				        		<td><?php echo "Rp.".$totalPrice.",00"; ?></td>
				        		<input type="hidden" name="totalPrice" value="<?php echo $totalPrice; ?>">
							</tr>
							<tr>
								<td><b>Email</b></td>
								<td><input type="text" name="email"></td>
								<td><b>Phone</b></td>
								<td><input type="text" name="phone"></td>
								<td><b>Address</b></td>
								<td><textarea name="address"></textarea></td>
							</tr>
							<tr>
								<td><b>Account Number</b></td>
								<td><input type="text" name="account"></td>
								<td><b>Bank Name</b></td>
								<td><input type="text" name="bankName"></td>
							</tr>
							<tr>
								<td colspan="5"></td>
								<td><input type="button" value="Pay Now" onclick="clickPayNow()"></td>
							</tr>
						</form>
					<?php
					} else {
					    echo "<font color=\"red\">There is no any payment.</font>";
					}
					?>
				</table>
				<?php $conn->close(); ?>
				<script type="text/javascript">
					function clickPayNow(){
						document.getElementById("formPayment").submit();
					}
				</script>
			</div>
		</div>
	</body>
</html>