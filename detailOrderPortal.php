<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<link rel="stylesheet" type="text/css" href="webStyle.css">
	</head>
	<body>
		<div>
			<header class="headerWeb">
				<div><h1>VOLLITIC</h1></div>
				<div><h3><u>DETAIL ORDER</u></h3></div>
				<div><script type="text/javascript">document.write(Date())</script></div>
			</header>
			<nav class="menuWeb">
				<button><a href="orderPortal.php">ORDER</a></button>
				<button><a href="shoppingCartPortal.php">SHOPPING CART</a></button>
				<button><a href="paymentPortal.php">PAYMENT</a></button>
				<button><a href="paidTransactionPortal.php">PAID TRANSACTION</a></button>
			</nav>
			<div class="containerWeb">
				<form action="handleOrder.php" method="post" id="formOrder">
					<?php 
						$code = $_POST["code"]; 
						include 'connection.php';
						$sql = "SELECT o.code, o.order_date, i.name, i.image, i.price, i.quantity, o.order_quantity 
						FROM `order` o JOIN items i 
						WHERE o.code = i.code AND o.code = ".$code."";
						$result=mysqli_query($conn,$sql);
					?>
					<table>
						<?php
						if ($result->num_rows > 0) {
						    // output data of each row
						    while($row=mysqli_fetch_assoc($result)) {
						    	echo "
						    	<tr>
						    		<td><img src=\"".$row["image"]."\"></td>
					        	</tr>
					        	<tr>
					        		<td><b>ID</b></td>
					        		<td>".$row["code"]."</td>
					        		<input type=\"hidden\" name=\"code\" value=\"".$row["code"]."\">
					        		<td><b>Order Date</b></td>
					        		<td>".$row["order_date"]."</td>
					        		<td><b>Name</b></td>
					        		<td>".$row["name"]."</td>
					        	</tr>
					        	<tr>
					        		<td><b>Price per Item</b></td>
					        		<td>Rp.".$row["price"].",00</td>
					        		<td><b>Order Quantity</b></td>
					        		<td><input type=\"text\" name=\"orderQuantity\" value=\"".$row["order_quantity"]."\"></td>
								</tr>
					        	<tr>
					        		<td><b>Sub Total</b></td>
					        		<td>Rp.".$row["price"]*$row["order_quantity"].",00</td>
					        		<td><b>Total Quantity</b></td>
					        		<td>".$row["quantity"]."</td>
					        	</tr>
					        	";
						    }
						} else {
						    echo "<font color=\"red\">There is no any order.</font>";
						}
						?>
					    <tr>
							<td><button><a href="shoppingCartPortal.php">Back</a></button></td>
							<input type="hidden" name="isDelete" id="isDelete">
							<td><input type="button" value="Submit Edit" onclick="clickEdit()"></td>
							<td colspan="2"><input type="button" value="Delete" onclick="clickDelete()"></td>
						</tr>
					</table>
					<script type="text/javascript">
						function clickEdit(){
							document.getElementById("formOrder").submit();
						}
						function clickDelete(){
							if(confirm("Are you sure want to delete this?")){
								document.getElementById("isDelete").value = "true";
								document.getElementById("formOrder").submit();	
							}
						}
					</script>
					<?php $conn->close(); ?>
				</form>
			</div>
		</div>
	</body>
</html>